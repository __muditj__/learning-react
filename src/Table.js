import React, {Component} from 'react'

//Creating a new component:
class Table extends Component {
    render() {
      const {characterData} = this.props
        return (
            <table>
                <TableHeader />
                <TableBody characterData={characterData} /> 
            </table>
        )
    }
}

//Adding some simple components
const TableHeader = () => {
    return (
        <thead>
          <tr>
            <th>Name</th>
            <th>Job</th>
          </tr>
        </thead>
        
    )
}

  const TableBody = (props) => {
    const rows = props.characterData.map((row, index) => {
      return (
        <tr key={index}>
          <td> {row.name} </td>
          <td> {row.job} </td>
        </tr>
      )
    })  
  console.log('Hi')
  console.log(typeof(rows))
  console.log(rows)
  return <tbody> {rows} </tbody> 
}



export default Table