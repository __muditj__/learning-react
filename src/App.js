import React from 'react';
import logo from './logo.svg';
import './App.css';

import Table from './Table'

//Function component
class App extends React.Component {
  state = {
    data: [
      {
        name: 'Charlie',
        job: 'Janitor',
      },
      {
        name: 'Mac',
        job: 'Bouncer',
      },
      {
        name: 'Dee',
        job: 'Aspring actress',
      },
      {
        name: 'Dennis',
        job: 'Bartender',
      },
    ]
  }
  
  removeCharacter = (index) => {
    const {data} = this.state

    //Setting state:
    this.setState({
      data: data.filter((character, ind) => {
        return ind !== index
      })
    })
  }


  //render methods determines what HTML will be rendered by a component
  render () {
    const {data} = this.state
    return (
    // This is JSX - JS embedded with HTML
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Table characterData={data} />
      </header>
    </div>
    );
  }
}

export default App;
